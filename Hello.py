from flask import Flask
from lxml import html
import requests

app = Flask(__name__)
@app.route('/')
def getProductDetails():
    headers = {
    'authority': 'www.amazon.in',
    'pragma': 'no-cache',
    'cache-control': 'no-cache',
    'rtt': '200',
    'downlink': '2.55',
    'ect': '4g',
    'sec-ch-ua': '^\\^Google',
    'sec-ch-ua-mobile': '?0',
    'upgrade-insecure-requests': '1',
    'user-agent': 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36',
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'sec-fetch-site': 'same-origin',
    'sec-fetch-mode': 'navigate',
    'sec-fetch-user': '?1',
    'sec-fetch-dest': 'document',
    'referer': 'https://www.amazon.in/computers-and-accessories/b/?ie=UTF8^&node=976392031^&ref_=nav_cs_pc',
    'accept-language': 'en-US,en;q=0.9',
    'cookie': 'session-id=260-3944287-8240025; ubid-acbin=258-0189211-0811067; x-acbin=^\\^djKTh1666aSFFyheWWc^@QUBTVI?XmIsyQ1ZxsZSz^@6dGnOAdvrip9b242QRUiE61^\\^; at-acbin=Atza^|IwEBIFOywCIvUB9OPtztZJeV8r6e5r0IKeuiU_eMuV8ssTMd0J0EX66Sn2g_1CrOt2GVyGjXLXDF-ASnnk7eb9yCnYQw2HT1bMwDIgpC3waqVXZk23raGqAQEso6WX0ZMH6gGRO0550KzrER-BXto3H9rznwLptYNmE_rmjqcq9w_4WtLlYY_XTm9Q27uRAOr_Aj63GDyDFAa0XC7P1fTNuanj_AmrPNYR0h9GPC4sOIlyylIA; sess-at-acbin=^\\^23A6qGeieMVN+ukdovQqhf/RLQowiai2rBX4E+rEBE4=^\\^; sst-acbin=Sst1^|PQGMVVjJivPFfBhvT99g_N9cCe7RxV_5F1dEQGp5WHlE97quOpH4WZ9tAv2Be5DizPTnfREKEXhH62x904NPSSVVJdSmhDIUPbQQrwJZdMxFKt3z4jyQTL5DRmxWGWypqIMlCk4dW6UCZLqXW-ZD1g_MwgHkPYoQ-uHEMptILeix1vdFchfY7uxIgTO_GXko4pI-jGRYHiwerbg_qzHyne-FmzDGYW5EJHw7v7czsER2LIVo06ZHWBZgX1f4bsotLTkDjiZw6ZpF1IdQi7X1v1RYeXA-njggBzffUTUy3y1gq78; session-token=^\\^zJ7pClnabQOIwFBsDe0AzR7EuIn9YPZ659iO113TX73ZoRB/McXanDgvh3sBee06eZtngiG7TbVJ1ej3u4wLJljjeFJjrRG9XXMvJLenQ5ax7zTLWZ+xD2HF/VeSGM8VI9maYckcAcizzoqfadiyQJBpYnSgskMtAgIRHNMwPDFhxRD6ktodZ1BtU9VDX6kvyJWsWmyr7zzD1TXUriKQLMj2F58+sj0Ynap3mt42fYoU+ucQ+bN67mJvBzNGx9XkKsm8hhzNnCjYJt3d/+Cv/g==^\\^; session-id-time=2082758401l; visitCount=5; csm-hit=tb:J51PMK05V3JSY3TQRTN2+s-W95NA9SHP28GE2S3C1SQ^|1609392703459^&t:1609392703482^&adb:adblk_no',
    }


    r=requests.get('https://www.amazon.in/gp/product/B01GFPGHSM/ref=s9_acss_bw_cg_header_3c1_w?pf_rd_m=A1K21FY43GMZF8^&pf_rd_s=merchandised-search-3^&pf_rd_r=53W4Z0R8QSV7YB4DES6N^&pf_rd_t=101^&pf_rd_p=22286308-8fbb-4c33-bff0-6c72698547db^&pf_rd_i=976392031', headers=headers)
    tree = html.fromstring(r.content)
    # res = r.status_code
    res = {}
    price = tree.xpath('//*[@id="priceblock_ourprice"]/text()')
    title = tree.xpath('//*[@id="productTitle"]/text()')

    price  = ''.join(price)
    title = ''.join(title)

    price = price.replace('\u20b9\u00a0','')
    title = title.replace('\n\n\n\n\n\n\n\n','')
    title = title.strip('\n\n\n\n\n\n\n')

    res['title'] = title
    res['price'] = price

    result = {}
    result['title'] = title
    result['price'] = price
    
    review = []
    
    names = tree.xpath("//span[@class='a-profile-name']/text()")
    stars = tree.xpath("//i[@data-hook='review-star-rating']/span[@class='a-icon-alt']/text()")
    reviewTitle = tree.xpath("//a[@data-hook='review-title']/span/text()")
    reviewDate = tree.xpath("//span[@data-hook='review-date']/text()")
    reviewData = tree.xpath("//span[@data-hook='format-strip-linkless']/text()")
    reviewDetails = tree.xpath("//div[@data-hook='review-collapsed']/span/text()")
    for rev in range(len(names)):
        reviewDict = {
            'name': names[rev],'review-title': reviewTitle[rev],'review-date': reviewDate[rev], 'review-Details': reviewDetails[rev]
            }
        review.append(reviewDict)
    result['review'] = str(review)
    return result

if __name__ == '__main__':
    app.run()